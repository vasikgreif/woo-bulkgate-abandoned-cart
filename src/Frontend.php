<?php

namespace WPProgramator\BulkGate\AbandonedCart;

class Frontend {
	/**
	 * @var Settings
	 */
	private $settings;

	/**
	 * @var AbandonedCart
	 */
	private $abandoned_cart;

	/**
	 * Frontend constructor.
	 *
	 * @param Settings $settings
	 * @param AbandonedCart $abandoned_cart
	 */
	public function __construct( Settings $settings, AbandonedCart $abandoned_cart ) {
		$this->settings       = $settings;
		$this->abandoned_cart = $abandoned_cart;
		$this->init();
	}

	/**
	 * Add actions and scripts
	 */
	private function init() {
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'wp_footer', array( $this, 'render_popup' ) );
	}

	public function enqueue_scripts() {
		wp_enqueue_style( 'featherlight', WOO_BULKGATE_ABANDONED_CART_URL . 'lib/featherlight/release/featherlight.min.css' );
		wp_enqueue_script( 'featherlight', WOO_BULKGATE_ABANDONED_CART_URL . 'lib/featherlight/release/featherlight.min.js', [ 'jquery' ] );
		wp_enqueue_style( 'woo-bulkgate-abandoned-cart', WOO_BULKGATE_ABANDONED_CART_URL . 'assets/css/woo-bulkgate-abandoned-cart.css' );
		wp_enqueue_script( 'woo-bulkgate-abandoned-cart', WOO_BULKGATE_ABANDONED_CART_URL . 'assets/js/woo-bulkgate-abandoned-cart.js', [ 'jquery' ] );

		wp_localize_script(
			'woo-bulkgate-abandoned-cart',
			'wbacVariables',
			[
				'restUrl'        => rest_url( WOO_BULKGATE_ABANDONED_CART_REST_NAMESPACE . '/' ),
				'popupDisplayed' => $this->abandoned_cart->get_popup_displayed_from_session(),
				'addedToCart'    => isset( $_POST['add-to-cart'] ),
				'validateString' => $this->settings->get_option( 'popup_validate_string' ) ?: false,
			]
		);
	}

	/**
	 * Render the popup
	 */
	public function render_popup() {
		?>
        <div id="woo-bulksms-abandoned-cart-popup">
            <form action="" method="post">
                <div>
					<?php if ( $heading = $this->settings->get_option( 'popup_heading' ) ): ?>
                        <h2><?php echo $heading; ?></h2>
					<?php endif; ?>
					<?php if ( $subheading = $this->settings->get_option( 'popup_subheading' ) ): ?>
                        <p><?php echo $subheading; ?></p>
					<?php endif; ?>

                    <input type="text" name="phone" placeholder="<?php echo $this->settings->get_option( 'popup_input_placehlder' ); ?>"/>
                </div>

                <div class="buttons">
                    <a href="#" class="btn disabled close-popup"><?php echo $this->settings->get_option( 'popup_button_continue_text' ); ?></a>
                    <a href="<?php echo wc_get_page_permalink( 'cart' ) ?>" class="btn disabled"><?php echo $this->settings->get_option( 'popup_button_cart_text' ); ?></a>
                    <a href="<?php echo wc_get_page_permalink( 'checkout' ) ?>" class="btn disabled"><?php echo $this->settings->get_option( 'popup_button_checkout_text' ); ?></a>
                </div>

            </form>
        </div>
	<?php }
}

