<?php
/*
Plugin Name: BulkGate Abandoned Cart
Description: BulkGate Abandoned cart
Author: Václav Greif
Author URI: https://wp-programator.cz/
Version: 1.0
Bitbucket Plugin URI: https://bitbucket.org/vasikgreif/targito-wp/
*/

namespace WPProgramator\BulkGate\AbandonedCart;

DEFINE( 'WOO_BULKGATE_ABANDONED_CART_URL', plugins_url( '/', __FILE__ ) );
define( 'WOO_BULKGATE_ABANDONED_CART_DIR', plugin_dir_path( __FILE__ ) );

require_once 'vendor/autoload.php';
new Loader();


register_activation_hook( __FILE__, 'WPProgramator\BulkGate\AbandonedCart\create_tables' );
/**
 * Create the db tables on plugin activation
 */
function create_tables() {
	global $wpdb, $jal_db_version;
	$table_name      = $wpdb->prefix . 'wbac_sessions';
	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name (
		  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
		  `hash` varchar(255) NOT NULL,
		  `products` varchar(500) NOT NULL,
		  `phone` varchar(255) NOT NULL,
		   `date` datetime NOT NULL,
		   `sms1_sent` int NOT NULL DEFAULT '0',
		   `sms2_sent` int NOT NULL DEFAULT '0'
	) $charset_collate;
	
	ALTER TABLE $table_name
	ADD INDEX `hash` (`hash`);
	";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

	add_option( 'jal_db_version', $jal_db_version );
}
