<?php

namespace WPProgramator\BulkGate\AbandonedCart;

/**
 * CMB2 Theme Options
 * @version 0.1.0
 */
class Settings {

	/**
	 * Holds an instance of the object
	 * @var Settings
	 */
	protected static $instance = null;
	/**
	 * Options Page title
	 * @var string
	 */
	protected $title = '';
	/**
	 * Options Page hook
	 * @var string
	 */
	protected $options_page = '';
	/**
	 * Option key, and option page slug
	 * @var string
	 */
	private $key = 'bulkgate_abandoned_cart_options';
	/**
	 * Options page metabox id
	 * @var string
	 */
	private $metabox_id = 'bulkgate_abandoned_cart_options_metabox';

	/**
	 * Constructor
	 * @since 0.1.0
	 */
	public function __construct() {
		// Set our title
		$this->title = __( 'BulkGate Abandoned Cart', 'bulkgate-abandoned-cart' );
		$this->hooks();
	}

	/**
	 * Initiate our hooks
	 * @since 0.1.0
	 */
	public function hooks() {
		add_action( 'admin_init', array( $this, 'init' ) );
		add_action( 'admin_menu', array( $this, 'add_options_page' ) );
		add_action( 'cmb2_admin_init', array( $this, 'add_options_page_metabox' ) );
	}


	/**
	 * Register our setting to WP
	 * @since  0.1.0
	 */
	public function init() {
		register_setting( $this->key, $this->key );
	}

	/**
	 * Add menu options page
	 * @since 0.1.0
	 */
	public function add_options_page() {

		$this->options_page = add_submenu_page( 'woocommerce', $this->title, $this->title, 'manage_options', 'admin.php?page=' . $this->key, array( $this, 'admin_page_display' ) );

		$this->options_page = add_menu_page( $this->title, $this->title, 'manage_options', $this->key, array( $this, 'admin_page_display' ) );
		remove_menu_page( $this->key );
		// Include CMB CSS in the head to avoid FOUC
		add_action( "admin_print_styles-{$this->options_page}", array( 'CMB2_hookup', 'enqueue_cmb_css' ) );
	}

	/**
	 * Admin page markup. Mostly handled by CMB2
	 * @since  0.1.0
	 */
	public function admin_page_display() {
		?>
        <div class="wrap cmb2-options-page <?php echo $this->key; ?>">
            <h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
			<?php cmb2_metabox_form( $this->metabox_id, $this->key ); ?>
        </div>
		<?php
	}

	/**
	 * Add the options metabox to the array of metaboxes
	 * @since  0.1.0
	 */
	public function add_options_page_metabox() {

		// hook in our save notices
		add_action( "cmb2_save_options-page_fields_{$this->metabox_id}", array( $this, 'settings_notices' ), 10, 2 );

		$cmb = new_cmb2_box(
			array(
				'id'         => $this->metabox_id,
				'hookup'     => false,
				'cmb_styles' => true,
				'show_on'    => array(
					// These are important, don't remove
					'key'   => 'options-page',
					'value' => array( $this->key ),
				),
			)
		);

		$cmb->add_field(
			array(
				'name' => __( 'BulkGate settings', 'woo-abandoned-cart' ),
				'id'   => 'bulkgate_title',
				'type' => 'title',
			)
		);
		$cmb->add_field(
			array(
				'name' => __( 'App ID', 'woo-abandoned-cart' ),
				'desc' => __( 'Enter the APP ID', 'woo-abandoned-cart' ),
				'id'   => 'bulkgate_app_id',
				'type' => 'text',
			)
		);
		$cmb->add_field(
			array(
				'name' => __( 'App Token', 'woo-abandoned-cart' ),
				'desc' => __( 'Enter the APP Token', 'woo-abandoned-cart' ),
				'id'   => 'bulkgate_app_token',
				'type' => 'text',
			)
		);
		$cmb->add_field(
			array(
				'name' => __( 'SMS 1 text', 'woo-abandoned-cart' ),
				'desc' => __( 'Enter the text for SMS, that will be sent after one hour if the checkout was not completed. Use placeholder {url} for the cart url', 'woo-abandoned-cart' ),
				'id'   => 'bulkgate_sms1_text',
				'type' => 'text',
			)
		);
		$cmb->add_field(
			array(
				'name' => __( 'SMS 1 delay', 'woo-abandoned-cart' ),
				'desc' => __( 'Enter the delay in minutes for this SMS 1', 'woo-abandoned-cart' ),
				'id'   => 'bulkgate_sms1_delay',
				'type' => 'text',
			)
		);
		$cmb->add_field(
			array(
				'name' => __( 'SMS 2 text', 'woo-abandoned-cart' ),
				'desc' => __( 'Enter the text for SMS, that will be sent after 24 hours if the checkout was not completed. Use placeholder {url} for the cart url', 'woo-abandoned-cart' ),
				'id'   => 'bulkgate_sms2_text',
				'type' => 'text',
			)
		);
		$cmb->add_field(
			array(
				'name' => __( 'SMS 2 delay', 'woo-abandoned-cart' ),
				'desc' => __( 'Enter the delay in minutes for this SMS 2', 'woo-abandoned-cart' ),
				'id'   => 'bulkgate_sms2_delay',
				'type' => 'text',
			)
		);

		$cmb->add_field(
			array(
				'name' => __( 'Unicode', 'woo-abandoned-cart' ),
				'desc' => __( 'Check to send Unicode messages', 'woo-abandoned-cart' ),
				'id'   => 'bulkgate_unicode',
				'type' => 'checkbox',
			)
		);

		$cmb->add_field(
			array(
				'name'    => __( 'Recovered orders', 'woo-abandoned-cart' ),
				'desc'    => __( 'This is a number of recovered orders', 'woo-abandoned-cart' ),
				'id'      => 'bulkgate_recovered_orders',
				'type'    => 'text',
				'default' => '0',
			)
		);

		$cmb->add_field(
			array(
				'name' => __( 'Popup settings', 'woo-abandoned-cart' ),
				'id'   => 'popup_settings',
				'type' => 'title',
			)
		);
		$cmb->add_field(
			array(
				'name' => __( 'Validate number start', 'woo-abandoned-cart' ),
				'desc' => __( 'Enter the string, that the number has to contain, ie. 05', 'woo-abandoned-cart' ),
				'id'   => 'popup_validate_string',
				'type' => 'text',
			)
		);
		$cmb->add_field(
			array(
				'name' => __( 'Popup heading', 'woo-abandoned-cart' ),
				'id'   => 'popup_heading',
				'type' => 'text',
			)
		);
		$cmb->add_field(
			array(
				'name' => __( 'Popup subheading', 'woo-abandoned-cart' ),
				'id'   => 'popup_subheading',
				'type' => 'text',
			)
		);
		$cmb->add_field(
			array(
				'name' => __( 'Phone input placeholder', 'woo-abandoned-cart' ),
				'id'   => 'popup_input_placeholder',
				'type' => 'text',
			)
		);
		$cmb->add_field(
			array(
				'name'    => __( 'Continue shopping', 'woo-abandoned-cart' ),
				'id'      => 'popup_button_continue_text',
				'type'    => 'text',
				'default' => __( 'Continue shopping', 'woo-abandoned-cart' ),
			)
		);
		$cmb->add_field(
			array(
				'name'    => __( 'Cart button text', 'woo-abandoned-cart' ),
				'id'      => 'popup_button_cart_text',
				'type'    => 'text',
				'default' => __( 'Go to cart', 'woo-abandoned-cart' ),
			)
		);
		$cmb->add_field(
			array(
				'name'    => __( 'Checkout button text', 'woo-abandoned-cart' ),
				'id'      => 'popup_button_checkout_text',
				'type'    => 'text',
				'default' => __( 'Go to checkout', 'woo-abandoned-cart' ),
			)
		);
	}


	/**
	 * Register settings notices for display
	 * @since  0.1.0
	 *
	 * @param  int $object_id Option key
	 * @param  array $updated Array of updated fields
	 *
	 * @return void
	 */
	public function settings_notices( $object_id, $updated ) {

		if ( $object_id !== $this->key || empty( $updated ) ) {
			return;
		}

		add_settings_error( $this->key . '-notices', '', __( 'Settings updated.', 'rm' ), 'updated' );
		settings_errors( $this->key . '-notices' );
	}

	/**
	 * Public getter method for retrieving protected/private variables
	 *
	 * @param $field
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public function __get( $field ) {
		// Allowed fields to retrieve
		if ( in_array( $field, array( 'key', 'metabox_id', 'title', 'options_page' ), true ) ) {
			return $this->{$field};
		}

		throw new \Exception( 'Invalid property: ' . $field );
	}

	/**
	 * Wrapper function around cmb2_get_option
	 * @since  0.1.0
	 *
	 * @param  string $key Options array key
	 * @param  mixed $default Optional default value
	 *
	 * @return mixed           Option value
	 */
	public function get_option( $key = '', $default = null ) {
		if ( function_exists( 'cmb2_get_option' ) ) {
			// Use cmb2_get_option as it passes through some key filters.
			return cmb2_get_option( $this->key, $key, $default );
		}

		// Fallback to get_option if CMB2 is not loaded yet.
		$opts = get_option( $this->key, $key, $default );

		$val = $default;

		if ( 'all' === $key ) {
			$val = $opts;
		} elseif ( is_array( $opts ) && array_key_exists( $key, $opts ) && false !== $opts[ $key ] ) {
			$val = $opts[ $key ];
		}

		return $val;
	}
}
