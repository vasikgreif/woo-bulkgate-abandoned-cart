<?php

namespace WPProgramator\BulkGate\AbandonedCart;

define( 'WOO_BULKGATE_ABANDONED_CART_REST_NAMESPACE', 'woo-bulkgate-abandoned-cart/v1' );

/**
 * Class Rest
 * @package WPProgramator\BulkGate\AbandonedCart
 */
class Rest extends \WP_REST_Controller {
	/**
	 * @var AbandonedCart
	 */
	private $abandoned_cart;

	public function __construct( AbandonedCart $abandoned_cart ) {
		$this->abandoned_cart = $abandoned_cart;
		add_action( 'rest_api_init', array( $this, 'register_routes' ) );

	}

	/**
	 * Register the routes for the objects of the controller.
	 */
	public function register_routes() {

		register_rest_route(
			WOO_BULKGATE_ABANDONED_CART_REST_NAMESPACE,
			'save-phone-number',
			array(
				array(
					'methods'  => \WP_REST_Server::CREATABLE,
					'callback' => array( $this, 'save_phone_number' ),
					'args'     => [
						'phone' => [
							'required' => true,
						],
					],

				),
			)
		);

		register_rest_route(
			WOO_BULKGATE_ABANDONED_CART_REST_NAMESPACE,
			'update-cart',
			array(
				array(
					'methods'  => \WP_REST_Server::CREATABLE,
					'callback' => array( $this, 'update_cart' ),
				),
			)
		);

		register_rest_route(
			WOO_BULKGATE_ABANDONED_CART_REST_NAMESPACE,
			'send-sms',
			array(
				array(
					'methods'  => \WP_REST_Server::READABLE,
					'callback' => array( $this, 'send_sms' ),
				),
			)
		);
	}

	/**
	 * Save phone number
	 *
	 * @param \WP_REST_Request $request Full data about the request.
	 *
	 * @return \WP_Error|\WP_REST_Request|\WP_REST_Response | bool
	 */
	public function save_phone_number( $request ) {
		$hash = $this->abandoned_cart->get_hash_from_session();
		if ( ! $hash ) {
			$hash = $this->abandoned_cart->generate_cookie_hash();
			$this->abandoned_cart->save_hash_to_session( $hash );
		}

		$phone = $request->get_param( 'phone' );
		$args  = [
			'hash'     => $hash,
			'products' => wp_json_encode( $this->abandoned_cart->get_cart_products() ),
			'phone'    => $phone,
			'date'     => date( 'Y-m-d H:i:s' ),
		];

		$result = $this->abandoned_cart->save_session( $args );

		if ( is_wp_error( $result ) ) {
			return $result;
		}

		$this->abandoned_cart->save_hash_to_session( $hash );
		$this->abandoned_cart->save_popup_displayed_to_session();
		WC()->customer->set_billing_phone( $phone );

		return new \WP_REST_Response(
			array(
				'status' => 'success',
				200,
			)
		);
	}

	/**
	 * Update cart
	 *
	 * @param \WP_REST_Request $request Full data about the request.
	 *
	 * @return \WP_Error|\WP_REST_Request|\WP_REST_Response | bool
	 */
	public function update_cart( $request ) {
		if ( ! $this->abandoned_cart->get_hash_from_session() ) {
			return;
		}

		$args = [
			'hash'     => $this->abandoned_cart->get_hash_from_session(),
			'products' => wp_json_encode( $this->abandoned_cart->get_cart_products() ),
			'date'     => date( 'Y-m-d H:i:s' ),
		];

		$result = $this->abandoned_cart->save_session( $args );

		if ( is_wp_error( $result ) ) {
			return $result;
		}

		return new \WP_REST_Response(
			array(
				'status' => 'success',
				200,
			)
		);
	}

	/**
	 * Send SMS
	 *
	 * @param \WP_REST_Request $request Full data about the request.
	 *
	 * @return \WP_Error|\WP_REST_Request|\WP_REST_Response | bool
	 */
	public function send_sms( $request ) {
		$this->abandoned_cart->send_sms();

		return new \WP_REST_Response(
			array(
				'status' => 'success',
				200,
			)
		);
	}

	/**
	 * Check if a given request has access to create items
	 *
	 * @param \WP_REST_Request $request Full data about the request.
	 *
	 * @return \WP_Error|bool
	 */
	public function create_item_permissions_check( $request ) {
		return true;
	}

	/**
	 * Prepare the item for the REST response
	 *
	 * @param mixed $item WordPress representation of the item.
	 * @param \WP_REST_Request $request Request object.
	 *
	 * @return mixed
	 */
	public function prepare_item_for_response( $item, $request ) {
		return array();
	}
}
