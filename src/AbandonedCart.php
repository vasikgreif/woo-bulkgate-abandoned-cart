<?php

namespace WPProgramator\BulkGate\AbandonedCart;

class AbandonedCart {
	/**
	 * @var string
	 */
	private $session_table;
	/**
	 * @var \wpdb
	 */
	private $db;
	/**
	 * @var BulkGate
	 */
	private $bulk_gate;
	/**
	 * @var Settings
	 */
	private $settings;

	/**
	 * AbandonedCart constructor.
	 *
	 * @param BulkGate $bulk_gate
	 * @param Settings $settings
	 */
	public function __construct( BulkGate $bulk_gate, Settings $settings ) {
		global $wpdb;
		$this->db            = $wpdb;
		$this->bulk_gate     = $bulk_gate;
		$this->settings      = $settings;
		$this->session_table = $this->db->prefix . 'wbac_sessions';

		$this->init();
	}

	/**
	 * Add actions
	 */
	private function init() {
		add_action( 'woocommerce_checkout_order_processed', array( $this, 'checkout_order_processed' ) );
		add_action( 'template_redirect', array( $this, 'maybe_restore_cart' ) );
	}

	/**
	 * Check if we should restore the cart
	 */
	public function maybe_restore_cart() {
		if ( isset( $_GET['wr'] ) && ! empty( $_GET['wr'] ) ) {
			$this->restore_cart( $_GET['wr'] );
		}
	}

	/**
	 * Restore cart
	 *
	 * @param $hash
	 *
	 * @throws \Exception
	 */
	private function restore_cart( $hash ) {
		$session = $this->get_session_by_hash( $hash );
		if ( ! $session ) {
			return;
		}

		WC()->cart->empty_cart();

		foreach ( json_decode( $session->products ) as $product ) {
			WC()->cart->add_to_cart( $product->product_id, $product->quantity, $product->variation_id );
		}

		$this->save_hash_to_session( $session->hash );
		$this->save_popup_displayed_to_session();
		$this->save_recovered_to_session();

		wp_safe_redirect( wc_get_page_permalink( 'cart' ) );
	}

	/**
	 * Get the session by hash
	 *
	 * @param $hash
	 *
	 * @return array|object|void|null
	 */
	public function get_session_by_hash( $hash ) {
		return $this->db->get_row(
			$this->db->prepare(
				"SELECT * from $this->session_table WHERE hash = %s",
				$hash
			)
		);
	}

	/**
	 * Save hash to the session
	 *
	 * @param $hash
	 */
	public function save_hash_to_session( $hash ) {
		WC()->session->set( 'wbac_hash', $hash );
	}

	/**
	 * Save the popup displayed to sesion
	 */
	public function save_popup_displayed_to_session() {
		WC()->session->set( 'wbac_popup_displayed', 1 );
	}

	/**
	 * Save the popup displayed to sesion
	 */
	public function save_recovered_to_session() {
		WC()->session->set( 'wbac_cart_recovered', 1 );
	}

	/**
	 * Clear session on checkout processed
	 */
	public function checkout_order_processed() {
		$this->maybe_update_recovered_orders_count();
		$this->clear_session();
	}

	public function maybe_update_recovered_orders_count() {
		if ( WC()->session->get( 'wbac_cart_recovered' ) ) {
			$settings                              = get_option( 'bulkgate_abandoned_cart_options' );
			$settings['bulkgate_recovered_orders'] = $settings['bulkgate_recovered_orders'] + 1;
			update_option( 'bulkgate_abandoned_cart_options', $settings );
		}
	}

	/**
	 * Clear the session data
	 */
	public function clear_session() {
		$this->delete_session( $this->get_hash_from_session() );
		WC()->session->__unset( 'wbac_hash' );
		WC()->session->__unset( 'wbac_popup_displayed' );
		WC()->session->__unset( 'wbac_hash' );
	}

	/**
	 * Delete session from db
	 *
	 * @param $hash
	 *
	 * @return false|int
	 */
	public function delete_session( $hash ) {
		$item = $this->get_session_by_hash( $hash );
		$this->db->delete(
			$this->session_table,
			[ 'phone' => $item->phone ]
		);

		return $this->db->delete(
			$this->session_table,
			[ 'hash' => $hash ]
		);
	}

	/**
	 * Get the hash from session
	 * @return array|string
	 */
	public function get_hash_from_session() {
		return WC()->session->get( 'wbac_hash', false );
	}

	/**
	 * Save the session to db
	 *
	 * @param $args
	 *
	 * @return $this|\WP_Error
	 */
	public function save_session( $args ) {
		if ( $this->get_session_by_hash( $args['hash'] ) ) {
			$result = $this->db->update( $this->session_table, $args, [ 'hash' => $args['hash'] ] );
		} else {
			$result = $this->db->insert( $this->session_table, $args );
		}

		if ( ! $result ) {
			return new \WP_Error( 'db-error', $this->db->last_error, $this->db->last_query );
		}

		return $this;
	}

	/**
	 * Get the products that are in cart
	 * @return array
	 */
	public function get_cart_products() {
		$products = [];
		foreach ( WC()->cart->get_cart_contents() as $item ) {
			$products[] = [
				'product_id'   => $item['product_id'],
				'variation_id' => $item['variation_id'],
				'quantity'     => $item['quantity'],
			];

		}

		return $products;
	}

	/**
	 * @return bool|string
	 */
	public function generate_cookie_hash() {

		$hash = substr( str_shuffle( str_repeat( '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMONQRSTUVWXYZ', 5 ) ), 0, 5 );
		if ( $this->db->get_var( "SELECT hash from $this->session_table WHERE hash = '$hash'" ) ) {
			$this->generate_cookie_hash();
		}

		return $hash;
	}

	/**
	 * Check if the popup was already displayed
	 * @return array|string
	 */
	public function get_popup_displayed_from_session() {
		return WC()->session->get( 'wbac_popup_displayed', false );
	}

	/**
	 * Send the SMS
	 */
	public function send_sms() {
		// Delete records older than 2 weeks
		$date = date( 'Y-m-d H:i:s', strtotime( 'now -2 weeks' ) );
		$this->db->get_results( "DELETE from $this->tableon_table WHERE date < '$date'" );

		// Find all SMS1 to be sent
		$delay1 = $this->settings->get_option( 'bulkgate_sms1_delay' );
		$date   = date( 'Y-m-d H:i:s', strtotime( "now -$delay1 minutes" ) );
		$sms1   = $this->db->get_results( "SELECT * FROM {$this->session_table} WHERE date < '$date' AND sms1_sent = 0" );
		foreach ( $sms1 as $sms ) {
			// Try to send the sms
			$result = $this->bulk_gate->send_sms( $sms->phone, str_replace( '{url}', $this->get_restore_cart_url( $sms->hash ), $this->settings->get_option( 'bulkgate_sms1_text' ) ) );
			if ( $result && ! is_wp_error( $result ) ) {
				$this->set_sms_as_sent( $sms->hash, 'sms1' );
			}
		}

		// Find all SMS2 to be sent
		$delay2 = $this->settings->get_option( 'bulkgate_sms2_delay' );
		$date   = date( 'Y-m-d H:i:s', strtotime( "now -$delay2 minutes" ) );
		$sms2   = $this->db->get_results( "SELECT * FROM {$this->session_table} WHERE date < '$date' AND sms2_sent = 0" );

		foreach ( $sms2 as $sms ) {
			// Try to send the sms
			$result = $this->bulk_gate->send_sms( $sms->phone, str_replace( '{url}', $this->get_restore_cart_url( $sms->hash ), $this->settings->get_option( 'bulkgate_sms2_text' ) ) );
			if ( $result && ! is_wp_error( $result ) ) {
				$this->set_sms_as_sent( $sms->hash, 'sms2' );
			}
		}
	}

	/**
	 * Get the restore cart URL
	 *
	 * @param $hash
	 *
	 * @return string
	 */
	private function get_restore_cart_url( $hash ) {
		return site_url( "?wr=$hash" );
	}

	/**
	 * Set SMS as sent
	 *
	 * @param $hash
	 * @param $sms_id
	 */
	public function set_sms_as_sent( $hash, $sms_id ) {
		$this->db->update(
			$this->session_table,
			[ $sms_id . '_sent' => 1 ],
			[ 'hash' => $hash ]
		);
	}
}
