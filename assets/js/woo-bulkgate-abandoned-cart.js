jQuery(document).ready(function ($) {
    var wooBulkgateAbandanedCart = {
        container: $('#woo-bulksms-abandoned-cart-popup'),
        phoneInput: $('input[name="phone"]', this.container),
        typingTimer: '',
        doneTypingInterval: 500,
        popupDisplayed: false,
        init: function () {
            $('body').on('added_to_cart', function () {
                wooBulkgateAbandanedCart.maybeDisplayPopup();
                wooBulkgateAbandanedCart.updateCart();
            });

            if (wbacVariables.addedToCart) {
                wooBulkgateAbandanedCart.maybeDisplayPopup();
            }

            $( document.body ).on( 'updated_wc_div', function(){
                wooBulkgateAbandanedCart.updateCart();
            });

            $('.btn', wooBulkgateAbandanedCart.container).on('click', function (e) {
                if ($(this).hasClass('disabled'))
                    e.preventDefault();
            });
            $('.btn.close-popup', wooBulkgateAbandanedCart.container).on('click', function (e) {
                if ($(this).hasClass('disabled'))
                    return;

                e.preventDefault();
                wooBulkgateAbandanedCart.closePopup();

            });


            wooBulkgateAbandanedCart.phoneInput.on('input', function () {
                clearTimeout(wooBulkgateAbandanedCart.typingTimer);
                var value = $(wooBulkgateAbandanedCart.phoneInput).val();

                if (!value) {
                    $('.btn', wooBulkgateAbandanedCart.container).addClass('disabled');
                    return;
                }
                if (wbacVariables.validateString && !value.match("^" + wbacVariables.validateString) )
                    return;

                if ($(wooBulkgateAbandanedCart.phoneInput).val()) {
                    wooBulkgateAbandanedCart.typingTimer = setTimeout(wooBulkgateAbandanedCart.savePhoneHumber, wooBulkgateAbandanedCart.doneTypingInterval);
                }
            });
        },
        maybeDisplayPopup: function () {
            if (!wbacVariables.popupDisplayed && !wooBulkgateAbandanedCart.popupDisplayed) {
                wooBulkgateAbandanedCart.displayPopup();
            }
        },
        displayPopup: function () {
            $.featherlight('#woo-bulksms-abandoned-cart-popup', {
                persist: true,
                closeOnClick: false,
                closeOnEsc: false,
                closeIcon: ''
            });
        },
        closePopup: function () {
            $.featherlight.close();
        },
        savePhoneHumber: function (e) {
            $.ajax({
                url: wbacVariables.restUrl + 'save-phone-number',
                method: 'POST',
                data: {
                    phone: wooBulkgateAbandanedCart.phoneInput.val()
                },
                success: function () {
                    $('.btn', wooBulkgateAbandanedCart.container).removeClass('disabled');
                    wooBulkgateAbandanedCart.popupDisplayed = true;
                }
            });
        },
        updateCart: function () {
            $.ajax({
                url: wbacVariables.restUrl + 'update-cart',
                method: 'POST'
            });
        }
    };

    wooBulkgateAbandanedCart.init();
});