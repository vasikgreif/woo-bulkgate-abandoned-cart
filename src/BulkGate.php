<?php

namespace WPProgramator\BulkGate\AbandonedCart;

class BulkGate {
	private $settings;
	private $sender;
	private $app_id;
	private $app_token;
	private $message = '';


	/** @var $response \BulkGate\Message\Response */
	private $response;

	/**
	 * BulkGate constructor.
	 *
	 * @param Settings $settings
	 */
	public function __construct( Settings $settings ) {
		$this->settings  = $settings;
		$this->app_id    = $this->settings->get_option( 'bulkgate_app_id' );
		$this->app_token = $this->settings->get_option( 'bulkgate_app_token' );
		$this->sender    = new \BulkGate\Sms\Sender(
			new \BulkGate\Message\Connection( $this->app_id, $this->app_token )
		);
		if ( $settings->get_option( 'bulkgate_unicode' ) === 'on' ) {
			$this->sender->unicode( true );
		}
	}

	/**
	 * Send the SMS
	 *
	 * @param $number
	 * @param $text
	 *
	 * @return \BulkGate\Message\Response|\WP_Error
	 */
	public function send_sms( $number, $text ) {
		$this->message = $text;
		try {

			$this->response = $this->sender->send( new \BulkGate\Sms\Message( $number, $this->message ) );
			/** @var $response \BulkGate\Message\Response */
			if ( ! $this->response->isSuccess() ) {
				return new \WP_Error( '500', $this->response->__get( 'error' ) );
			}

			return $this->response;

		} catch ( \Exception $e ) {
			return new \WP_Error( '500', $e->getMessage() );
		}
	}

	/**
	 * Check if the phone number is valid
	 *
	 * @param $number
	 *
	 * @return bool|\BulkGate\Message\Response|\WP_Error
	 */
	public function is_phone_number_valid( $number ) {
		$result = $this->validate_phone_number( $number );
		if ( is_wp_error( $result ) ) {
			return $result;
		}

		$number = (int) filter_var( $number, FILTER_SANITIZE_NUMBER_INT );

		if ( isset( $result->{$number} ) && isset( $result->{$number}['area_found'] ) ) {
			return (bool) $result->{$number}['area_found'];
		}

		return false;
	}

	/**
	 * Validate phone number
	 *
	 * @param $number
	 *
	 * @return \BulkGate\Message\Response|\WP_Error
	 */
	public function validate_phone_number( $number ) {
		try {
			return $this->sender->checkPhoneNumbers( $number );
		} catch ( \Exception $e ) {
			return new \WP_Error( '500', $e->getMessage() );
		}
	}


}