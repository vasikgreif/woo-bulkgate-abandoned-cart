<?php

namespace WPProgramator\BulkGate\AbandonedCart;

class Loader {
	/**
	 * @var BulkGate
	 */
	public $bulkgate;
	/**
	 * @var Settings
	 */
	public $settings;

	public $abandoned_cart;

	public function __construct() {
		$this->settings       = new Settings();
		$this->bulkgate       = new BulkGate( $this->settings );
		$this->abandoned_cart = new AbandonedCart( $this->bulkgate, $this->settings );
		new Rest( $this->abandoned_cart );
		new Frontend( $this->settings, $this->abandoned_cart );
	}
}